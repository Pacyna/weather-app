import axios from 'axios';

const result = document.querySelector('.result-container');
const click = document.querySelector('.search-button');

const paramsObject = {
    params: {
        q: 'London,uk',
        APPID: 'b6c4cb73082c44a9dbdb741a1e48aa71'
    }
};

const loadApi = () => {
    axios.get('https://api.openweathermap.org/data/2.5/weather', paramsObject)
        .then((response) => {
            const resultApi = response.data;
            console.log(resultApi);
        });
};

click.addEventListener('click', () => {
    loadApi();
});

result.addEventListener('enter', () =>{

});
